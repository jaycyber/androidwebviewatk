<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->form_validation->set_rules('transaksi', 'Transaksi', 'required|in_list[barang_masuk,barang_keluar]');
        $this->form_validation->set_rules('tanggal', 'Periode Tanggal', 'required');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Laporan Transaksi";
            $this->template->load('templates/dashboard', 'laporan/form', $data);
        } else {
            $this->load->library('CustomPDF');
            $input = $this->input->post(null, true);
            $table = $input['transaksi'];
            $tanggal = $input['tanggal'];
            $pecah = explode(' - ', $tanggal);
            $mulai = date('Y-m-d', strtotime($pecah[0]));
            $akhir = date('Y-m-d', strtotime(end($pecah)));

            $query = '';
            if ($table == 'barang_masuk') {
                $pdf = new FPDF();
                $pdf->AddPage('P', 'Letter');
                $pdf->SetFont('Times', 'B', 16);
                $pdf->Cell(190, 7, 'Laporan Barang Masuk', 0, 1, 'C');
                $pdf->SetFont('Times', '', 10);
                $pdf->Cell(190, 4, 'Tanggal : ' . $tanggal, 0, 1, 'C');
                $pdf->Ln(10);

                $pdf->SetFont('Arial', 'B', 10);

                $pdf->Cell(10, 7, 'No.', 1, 0, 'C');
                $pdf->Cell(25, 7, 'Tgl Masuk', 1, 0, 'C');
                $pdf->Cell(35, 7, 'ID Transaksi', 1, 0, 'C');
                $pdf->Cell(55, 7, 'Nama Barang', 1, 0, 'C');
                $pdf->Cell(40, 7, 'Supplier', 1, 0, 'C');
                $pdf->Cell(30, 7, 'Jumlah Masuk', 1, 0, 'C');
                $pdf->Ln();

                $query = $this->admin->getBarangMasuk(null, null, ['mulai' => $mulai, 'akhir' => $akhir]);
                $no = 1;
                foreach ($query as $d) {
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(10, 7, $no++ . '.', 1, 0, 'C');
                    $pdf->Cell(25, 7, $d['tanggal_masuk'], 1, 0, 'C');
                    $pdf->Cell(35, 7, $d['id_barang_masuk'], 1, 0, 'C');
                    $pdf->Cell(55, 7, $d['nama_barang'], 1, 0, 'L');
                    $pdf->Cell(40, 7, $d['nama_supplier'], 1, 0, 'L');
                    $pdf->Cell(30, 7, $d['jumlah_masuk'] . ' ' . $d['nama_satuan'], 1, 0, 'C');
                    $pdf->Ln();  
                }
                $file_name = 'Barang Masuk' . ' ' . $tanggal;
                $pdf->Output('I', $file_name);    

            } else {
               
                $pdf = new FPDF();
                
                $pdf->AddPage('P', 'Letter');
                $pdf->SetFont('Times', 'B', 16);
                $pdf->Cell(190, 7, 'Laporan Barang Keluar', 0, 1, 'C');
                $pdf->SetFont('Times', '', 10);
                $pdf->Cell(190, 4, 'Tanggal : ' . $tanggal, 0, 1, 'C');
                $pdf->Ln(10);

                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(10, 7, 'No.', 1, 0, 'C');
                $pdf->Cell(25, 7, 'Tgl Keluar', 1, 0, 'C');
                $pdf->Cell(35, 7, 'ID Transaksi', 1, 0, 'C');
                $pdf->Cell(95, 7, 'Nama Barang', 1, 0, 'C');
                $pdf->Cell(30, 7, 'Jumlah Keluar', 1, 0, 'C');
                $pdf->Ln();

                $query = $this->admin->getBarangKeluar(null, null, ['mulai' => $mulai, 'akhir' => $akhir]);
                $no = 1;
                foreach ($query as $d) {
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(10, 7, $no++ . '.', 1, 0, 'C');
                    $pdf->Cell(25, 7, $d['tanggal_keluar'], 1, 0, 'C');
                    $pdf->Cell(35, 7, $d['id_barang_keluar'], 1, 0, 'C');
                    $pdf->Cell(95, 7, $d['nama_barang'], 1, 0, 'L');
                    $pdf->Cell(30, 7, $d['jumlah_keluar'] . ' ' . $d['nama_satuan'], 1, 0, 'C');
                    $pdf->Ln();
                }
                $file_name = 'Barang Keluar' . ' ' . $tanggal;
                $pdf->Output('I',$file_name,true);
                
                          
            }
        }
    }

    
}
