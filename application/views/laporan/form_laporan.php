<div class="row justify-content-center">
    <div class="col-lg-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Form Laporan
                </h4>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?> 
                <label class="col-md-6  text-md-right" for="transaksi">Laporan Transaksi</label>               
                <div class="row form-group">   
                        <div class="col-md-6 col-lg-6 offset-lg-1 pb-4">
                        <form class="barang_masuk" method="#" action="<?= base_url('laporan/cetak_laporan_bm') ?>">
                            <button type="submit" class="btn btn-primary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-print"></i>
                                </span>
                                <span class="text" > 
                                   Barang Masuk
                                </span></a>
                            </button>
                        </form>
                        </div>
                        <div class="col-md-8 col-lg-6 offset-lg-1">
                        <form class="barang_masuk" method="#" action="<?= base_url('laporan/cetak_laporan_bk') ?>">
                            <button type="submit"  class="btn btn-primary btn-icon-split">
                                <span class="icon">
                                    <i class="fa fa-print"></i>
                                </span>
                                <span class="text" > 
                                    Barang Keluar
                                </span></a>
                            </button>
                        </form>
                        </div>

                </div>
                
                   
               
                
            </div>
        </div>
    </div>
</div>