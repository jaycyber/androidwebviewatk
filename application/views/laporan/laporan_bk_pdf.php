<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title></title>
        <style>
            #table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #table td, #table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #table tr:nth-child(even){background-color: #f2f2f2;}

            #table tr:hover {background-color: #ddd;}

            #table th {
                padding-top: 10px;
                padding-bottom: 10px;
                text-align: left;
                background-color: #4e73df;
                color: white;
            }
        </style>
    </head>
    <body>
        <div style="text-align:center">
            <h1> Laporan Barang Keluar</h1>
            <br><br>
        </div>  
        <table id="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal Keluar</th>
                    <th>ID Transaksi</th>
                    <th>Nama Barang</th>
                    <th>Nama Pengambil</th>
                    <th>Bagian</th>
                    <th>Jumlah Keluar</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                    $no = 1;
                    foreach ($laporan as $d) :
                        
                ?>
                <tr>
                    <td scope="row"><?= $no++?></td>
                    <td><?= $d['tanggal_keluar'] ?></td>
                    <td><?= $d['id_barang_keluar']?></td>
                    <td><?= $d['nama_barang']?></td>
                    <td><?= $d['nama']?></td>
                    <td><?= $d['bagian']?></td>
                    <td><?= $d['jumlah_keluar']. ' ' . $d['nama_satuan'] ?></td>
                </tr>
                <?php endforeach;?>
              
                
            </tbody>
            
        </table>
    </body>
</html>