<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title></title>
        <style>
            #table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #table td, #table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #table tr:nth-child(even){background-color: #f2f2f2;}

            #table tr:hover {background-color: #ddd;}

            #table th {
                padding-top: 10px;
                padding-bottom: 10px;
                text-align: left;
                background-color: #4e73df;
                color: white;
            }
        </style>
    </head>
    <body>
        <div style="text-align:center">
            <h1> Laporan Barang Masuk</h1>
            <br><br>
        </div>  
        <table id="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Tanggal Masuk</th>
                    <th>ID Transaksi</th>
                    <th>Nama Barang</th>
                    <th>Supplier</th>
                    <th>Jumlah Masuk</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                    $no = 1;
                    foreach ($laporan as $d) :
                ?>
                <tr>
                    <td scope="row"><?= $no++?></td>
                    <td><?= $d['tanggal_masuk'] ?></td>
                    <td><?= $d['id_barang_masuk']?></td>
                    <td><?= $d['nama_barang']?></td>
                    <td><?= $d['nama_supplier']?></td>
                    <td><?= $d['jumlah_masuk']. ' ' . $d['nama_satuan'] ?></td>
                </tr>
                <?php endforeach;?>
              
                
            </tbody>
            
        </table>
    </body>
</html>